# TimeFlow: Adjustable Interval Stopwatch

Welcome to TimeFlow, an adjustable interval stopwatch application built using React, Vite, and Tailwind CSS. TimeFlow allows you to create a versatile stopwatch with customizable time intervals, and a beautiful user interface.

![TimeFlow Screenshot](screenshot.png)

## Features

- Adjustable stopwatch time in seconds.
- Customizable counting interval in seconds.
- Real-time display of elapsed time.
- Start, stop, reset, and lap actions.
- Keyboard shortcuts for convenient control.

## Getting Started

Follow these steps to set up and run the TimeFlow Adjustable Interval Stopwatch on your local machine

### 1. Clone the repository:
   ```bash
   git clone https://gitlab.com/davoood/zamin-coding-challenge-1.git
   cd zamin-coding-challenge-1
   ```

### 2. Run the project:
#### If using Docker:
- Just run `docker-compose up -d`

#### If not using Docker:
- Install dependencies by `npm install` or `yarn install`
- Run tests by `npx jest` or `yarn run jest`
- Build the project by `npm run build` or `yarn build`

### 3. Show the project:
- Move `/dist` files to your destination server folder or run `npm run dev` or run `yarn dev`
- Open your web browser and go to `http://localhost:8000` or `http://<your_ip>:8000` to access the TimeFlow application.


##Customization

You can customize the TimeFlow UI by adjusting the Tailwind CSS classes in the components. The color palette and responsive design are carefully chosen to provide a visually appealing experience.


####Built with ❤️ by Davood Feridoni