FROM node:18.16-alpine


WORKDIR /app

COPY package*.json  ./

RUN npm install -g yarn --force

RUN yarn install

COPY . .

EXPOSE 8000
RUN chmod +x Docker/entrypoint.sh
CMD [ "sh", "Docker/entrypoint.sh" ]
