import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import ToggleDarkMode from '../../../components/common/ToggleDarkMode';

import '@testing-library/jest-dom/extend-expect';

describe('Toggle Dark Mode Component', () => {
    it('render toggle dark mode', () => {
        render(<ToggleDarkMode />);
        expect(screen.getByAltText(/Dark Mode/i)).toBeInTheDocument();
    });

    it('click toggle dark mode', () => {
        render(<ToggleDarkMode />);

        const toggleBtn = screen.getByAltText(/Dark Mode/i);

        expect(document.documentElement.classList.toString()).toContain('dark');

        fireEvent.click(toggleBtn);

        expect(document.documentElement.classList.toString()).toBe('');

        fireEvent.click(toggleBtn);

        expect(document.documentElement.classList.toString()).toContain('dark');
    });
});
