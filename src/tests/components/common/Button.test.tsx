import React from 'react';
import { render, screen } from '@testing-library/react';
import Button from '../../../components/common/Button';

import '@testing-library/jest-dom/extend-expect';

describe('Button Component', () => {
    it('render button', () => {
        render(<Button text="testBtn" onClick={() => console.log('btn test')} />);
        expect(screen.getByText(/testBtn/i)).toBeInTheDocument();
    });

    it('render disable button', () => {
        render(<Button text="testDisableBtn" onClick={() => console.log('btn test')} disabled />);
        expect(screen.getByText(/testDisableBtn/i)).toBeInTheDocument();
        expect(screen.getByText(/testDisableBtn/i)).toHaveAttribute('disabled');
    });
});
