import React from 'react';
import { render, screen } from '@testing-library/react';
import ImageButton from '../../../components/common/ImageButton';

import '@testing-library/jest-dom/extend-expect';

describe('Image Button Component', () => {
    it('render image button', () => {
        render(
            <ImageButton
                imageSrc="/moon.svg"
                imageAlt="testImgAlt"
                text="testImgBtn"
                onClick={() => console.log('btn test')}
            />
        );
        expect(screen.getByText(/testImgBtn/i)).toBeInTheDocument();
        expect(screen.getByAltText(/testImgAlt/i)).toHaveAttribute('src', '/moon.svg');
    });

    it('render image button without text', () => {
        render(<ImageButton imageSrc="/moon.svg" imageAlt="testImgAlt" onClick={() => console.log('btn test')} />);
        expect(screen.getByAltText(/testImgAlt/i)).toBeInTheDocument();
        expect(screen.getByAltText(/testImgAlt/i)).toHaveAttribute('src', '/moon.svg');
    });
});
