import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from '../../../components/common/Header';

import '@testing-library/jest-dom/extend-expect';

describe('Header Component', () => {
    it('render header', () => {
        render(<Header />);
        expect(screen.getByTestId('header')).toBeInTheDocument();
    });
});
