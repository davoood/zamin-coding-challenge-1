import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import StopwatchCard from '../../../components/stopwatch/StopwatchCard';

import '@testing-library/jest-dom/extend-expect';

describe('StopwatchCard Component', () => {
    it('render stopwatch card', () => {
        render(<StopwatchCard />);
        expect(screen.getByRole('main')).toBeInTheDocument();
    });

    it('start button works', async () => {
        render(<StopwatchCard />);

        const startBtn = screen.getByTestId('btn-Start');
        const showTimer = screen.getByTestId('showTimer');

        expect(showTimer).toHaveTextContent(/00:00:00/i);

        fireEvent.click(startBtn);

        setTimeout(() => {
            expect(showTimer).toHaveTextContent(/00:00:01/i);
        }, 1000);
    });

    it('stop button works', async () => {
        render(<StopwatchCard />);

        const startBtn = screen.getByTestId('btn-Start');
        const showTimer = screen.getByTestId('showTimer');

        expect(showTimer).toHaveTextContent(/00:00:00/i);

        fireEvent.click(startBtn);
        const stopBtn = screen.getByTestId('btn-Stop');

        expect(stopBtn).toBeInTheDocument();
        fireEvent.click(stopBtn);

        setTimeout(() => {
            expect(showTimer).toHaveTextContent(/00:00:00/i);
        }, 1000);
    });

    it('reset button works', async () => {
        render(<StopwatchCard />);

        const startBtn = screen.getByTestId('btn-Start');
        const resetBtn = screen.getByTestId('btn-Reset');
        const stopwatchTimeInput = screen.getByTestId('stopwatchTime') as HTMLInputElement;
        const intervalTimeInput = screen.getByTestId('intervalTime') as HTMLInputElement;
        const showTimer = screen.getByTestId('showTimer');

        fireEvent.change(stopwatchTimeInput, { target: { value: '2' } });
        fireEvent.change(intervalTimeInput, { target: { value: '2' } });

        expect(showTimer).toHaveTextContent(/00:00:02/i);

        fireEvent.click(startBtn);

        setTimeout(() => {
            expect(showTimer).toHaveTextContent(/00:00:04/i);
            fireEvent.click(resetBtn);
            expect(showTimer).toHaveTextContent(/00:00:00/i);
            expect(stopwatchTimeInput.value).toBe('0');
            expect(intervalTimeInput.value).toBe('1');
        }, 2000);
    });

    it('lap button works', async () => {
        render(<StopwatchCard />);

        const startBtn = screen.getByTestId('btn-Start');
        const lapBtn = screen.getByTestId('btn-Lap');

        expect(lapBtn).toHaveAttribute('disabled');

        fireEvent.click(startBtn);

        expect(lapBtn).not.toHaveAttribute('disabled');

        fireEvent.click(lapBtn);
        const lapsList = screen.getByTestId('laps-list');

        const liElements = lapsList.querySelectorAll('li');

        expect(liElements.length).toBe(1);
    });
});
