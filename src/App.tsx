import React from 'react';
import Header from './components/common/Header';
import StopwatchCard from './components/stopwatch/StopwatchCard';

function App() {
    return (
        <div className="flex flex-col items-center justify-center min-h-screen bg-blue-900 dark:bg-gray-900 backdrop-blur">
            <Header />
            <StopwatchCard />
        </div>
    );
}

export default App;
