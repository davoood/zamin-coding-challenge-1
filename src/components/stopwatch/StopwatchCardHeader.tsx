import React, { memo } from 'react';

const StopwatchCardHeader = () => {
    return (
        <div className="text-center mb-6">
            <h1 className="text-3xl font-bold text-blue-500 dark:text-blue-300">
                <span className="animate__animated animate__bounce animate__infinite">
                    Adjustable Interval Stopwatch
                </span>
            </h1>
            <p className="font-bold py-2 mt-2 border-gradient  bg-gradient-to-r from-blue-500 to-purple-500 text-transparent bg-clip-text">
                Embrace the rhythm of time.
            </p>
        </div>
    );
};

export default memo(StopwatchCardHeader);
