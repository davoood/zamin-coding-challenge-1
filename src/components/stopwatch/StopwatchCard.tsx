import React, { useEffect, useRef, useState } from 'react';
import ToggleDarkMode from '../common/ToggleDarkMode';
import StopwatchCardHeader from './StopwatchCardHeader';
import StopwatchCardInputs, { StopwatchCardInputsRef } from './StopwatchCardInputs';
import StopwatchCardActions from './StopwatchCardActions';
import { formatTime } from '../../helpers/common';
import StopwatchCardLaps from './StopwatchCardLaps';

const StopwatchCard = () => {
    const [stopwatchTime, setStopwatchTime] = useState<number>(0);
    const [intervalTime, setIntervalTime] = useState<number>(1);
    const [running, setRunning] = useState<boolean>(false);
    const [lapTimes, setLapTimes] = useState<Array<{ date: string; time: number }>>([]);

    const intervalRef = useRef<number | null>(null);
    const inputsRef = useRef<StopwatchCardInputsRef | null>(null);

    const startStopwatch = () => {
        if (!running) {
            intervalRef.current = window.setInterval(() => {
                setStopwatchTime((prevTime) => prevTime + intervalTime);
            }, intervalTime * 1000);
        } else {
            window.clearInterval(intervalRef.current || undefined);
        }
        setRunning(!running);
    };

    const resetStopwatch = () => {
        window.clearInterval(intervalRef.current || undefined);
        setStopwatchTime(0);
        setIntervalTime(1);
        setRunning(false);
        setLapTimes([]);
        if (inputsRef.current) {
            inputsRef.current!.setStopwatchTimeValue(0);
            inputsRef.current!.setIntervalTimeValue(1);
        }
    };

    const recordLap = () => {
        if (running) {
            setLapTimes((prevLapTimes) => [
                ...prevLapTimes,
                {
                    date: new Date().toLocaleString('en', { dateStyle: 'medium', timeStyle: 'medium' }),
                    time: stopwatchTime,
                },
            ]);
        }
    };

    // Add a global event listener for keyboard shortcuts
    useEffect(() => {
        const handleKeyDown = (event: KeyboardEvent) => {
            switch (event.keyCode) {
                case 83:
                    startStopwatch();
                    break;
                case 76:
                    if (running) {
                        recordLap();
                    }
                    break;
                case 82:
                    resetStopwatch();
                    break;
                default:
                    break;
            }
        };

        window.addEventListener('keydown', handleKeyDown);
        return () => {
            window.removeEventListener('keydown', handleKeyDown);
        };
    }, [running]);

    return (
        <div
            role="main"
            className="bg-white dark:bg-gray-800 shadow-md rounded-lg p-6 w-full sm:w-4/5 md:w-3/5 lg:w-2/5 xl:w-1/3 animate__animated animate__fadeIn animate__delay-2s"
        >
            <ToggleDarkMode />
            <StopwatchCardHeader />
            <StopwatchCardInputs
                intervalTime={intervalTime}
                stopwatchTime={stopwatchTime}
                setIntervalTime={setIntervalTime}
                setStopwatchTime={setStopwatchTime}
                ref={inputsRef}
            />

            {/* Show Timer */}
            <div className="mb-8 text-3xl text-center text-blue-500 dark:text-blue-300" data-testid="showTimer">
                <div className="timer-animation">
                    <span className="timer-text">{formatTime(stopwatchTime)}</span>
                </div>
            </div>

            <StopwatchCardActions
                running={running}
                startStopwatch={startStopwatch}
                resetStopwatch={resetStopwatch}
                recordLap={recordLap}
            />

            {lapTimes && lapTimes.length > 0 && (
                <>
                    <div className="inline-flex items-center justify-center w-full">
                        <hr className="w-64 h-1 my-8 bg-gray-200 border-0 rounded dark:bg-gray-700" />
                    </div>
                    <StopwatchCardLaps lapTimes={lapTimes} />
                </>
            )}
        </div>
    );
};

export default StopwatchCard;
