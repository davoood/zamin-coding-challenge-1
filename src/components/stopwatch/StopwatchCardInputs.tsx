import React, { useRef, forwardRef, useImperativeHandle } from 'react';

interface StopwatchCardInputsProps {
    stopwatchTime: number;
    intervalTime: number;
    setStopwatchTime: React.Dispatch<React.SetStateAction<number>>;
    setIntervalTime: React.Dispatch<React.SetStateAction<number>>;
}

export interface StopwatchCardInputsRef {
    setStopwatchTimeValue: (value: number) => void;
    setIntervalTimeValue: (value: number) => void;
}

const StopwatchCardInputs = forwardRef<StopwatchCardInputsRef, StopwatchCardInputsProps>(
    ({ stopwatchTime, intervalTime, setStopwatchTime, setIntervalTime }, ref) => {
        const stopwatchTimeRef = useRef<HTMLInputElement | null>(null);
        const countingIntervalRef = useRef<HTMLInputElement | null>(null);

        useImperativeHandle(ref, () => ({
            setStopwatchTimeValue: (value) => {
                if (stopwatchTimeRef.current) {
                    stopwatchTimeRef.current!.value = value.toString();
                }
            },
            setIntervalTimeValue: (value) => {
                if (countingIntervalRef.current) {
                    countingIntervalRef.current!.value = value.toString();
                }
            },
        }));

        return (
            <div>
                <div className="mb-6">
                    <label className="block text-gray-500 dark:text-gray-400 mb-1">Stopwatch Time (s)</label>
                    <input
                        type="number"
                        defaultValue={stopwatchTime}
                        onChange={(e) => setStopwatchTime(Math.abs(parseInt(e.target.value)))}
                        className="border text-center border-gray-300 dark:border-gray-600 p-3 w-full rounded-md focus:outline-none focus:ring focus:border-blue-500 "
                        ref={stopwatchTimeRef}
                        data-testid="stopwatchTime"
                    />
                </div>
                <div className="mb-6">
                    <label className="block text-gray-500 dark:text-gray-400 mb-1">Counting Interval (s)</label>
                    <input
                        type="number"
                        defaultValue={intervalTime}
                        onChange={(e) => setIntervalTime(Math.abs(parseInt(e.target.value)))}
                        className="border text-center border-gray-300 dark:border-gray-600 p-3 w-full  rounded-md focus:outline-none focus:ring focus:border-blue-500 "
                        ref={countingIntervalRef}
                        data-testid="intervalTime"
                    />
                </div>
            </div>
        );
    }
);

export default StopwatchCardInputs;
