import React, { FC, memo } from 'react';
import Button from '../common/Button';

interface StopwatchCardActionsProps {
    running: boolean;
    startStopwatch: () => void;
    resetStopwatch: () => void;
    recordLap: () => void;
}

const StopwatchCardActions: FC<StopwatchCardActionsProps> = ({
    running,
    startStopwatch,
    resetStopwatch,
    recordLap,
}) => {
    const keyboardShortcutStyles = {
        parent: 'text-gray-500 dark:text-gray-400 pb-2 w-[300px]',
        key: 'px-2 py-1.5 text-xs text-gray-800 bg-gray-100 border border-gray-200 rounded-lg dark:bg-gray-600 dark:text-gray-100 dark:border-gray-500',
    };

    return (
        <div className="flex flex-col justify-center items-center">
            <div className="flex justify-center space-x-4 animate__animated animate__fadeIn animate__delay-7s">
                <Button
                    text={running ? 'Stop' : 'Start'}
                    onClick={startStopwatch}
                    classes={running ? 'bg-red-400' : 'bg-green-600'}
                />

                <Button text="Reset" onClick={resetStopwatch} classes="bg-blue-600" />

                <Button
                    text="Lap"
                    onClick={recordLap}
                    classes={running ? 'bg-blue-800' : 'bg-gray-400 cursor-not-allowed'}
                    disabled={!running}
                />
            </div>
            <div className="inline-flex items-center justify-center w-full">
                <hr className="w-64 h-1 my-8 bg-gray-200 border-0 rounded dark:bg-gray-700" />
                <div className="absolute px-4 -translate-x-1/2 bg-white left-1/2 ">or</div>
            </div>

            <p className={keyboardShortcutStyles.parent}>
                Please press <kbd className={keyboardShortcutStyles.key}>S</kbd> to start/stop stopwatch.
            </p>

            <p className={keyboardShortcutStyles.parent}>
                Please press <kbd className={keyboardShortcutStyles.key}>R</kbd> to reset stopwatch.
            </p>

            <p className={keyboardShortcutStyles.parent}>
                Please press <kbd className={keyboardShortcutStyles.key}>L</kbd> to record a lap stopwatch.
            </p>
        </div>
    );
};

export default memo(StopwatchCardActions);
