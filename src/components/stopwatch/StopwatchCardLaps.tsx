import React, { FC, memo } from 'react';
import { copyTextToClipboard, formatTime } from '../../helpers/common';
import ImageButton from '../common/ImageButton';

interface StopwatchCardLapsProp {
    lapTimes: Array<{ date: string; time: number }>;
}

const StopwatchCardLaps: FC<StopwatchCardLapsProp> = ({ lapTimes }) => {
    return (
        <div className="pt-4 mb-4 max-h-[200px] overflow-scroll">
            <div className="text-xl mb-2 text-dark dark:text-white">⏱️Lap Times:</div>
            <ul className="pl-6" data-testid="laps-list">
                {lapTimes.map((lapTime, index) => (
                    <li key={index} className="flex items-center text-dark dark:text-white my-2">
                        <span className="bg-gray-400 dark:bg-gray-600 rounded-full py-1 px-2 mr-1">{index + 1}</span>
                        <span className="text-base pr-1">{formatTime(lapTime.time)}</span>
                        <span className="font-bold">({lapTime.date})</span>

                        <ImageButton
                            onClick={() => copyTextToClipboard(`${lapTime.date} - ${formatTime(lapTime.time)}`)}
                            classes="ml-2 px-1 py-1 bg-blue-500 text-white text-xs rounded-md hover:bg-blue-600 transition-colors rounded-full"
                            imageSrc="/share.svg"
                            imageAlt="share"
                            imageTitle="share"
                            imageWidth={20}
                            imageHeight={20}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default memo(StopwatchCardLaps);
