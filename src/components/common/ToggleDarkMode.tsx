import React, { useCallback, useEffect, useState, memo } from 'react';
import ImageButton from './ImageButton';

const ToggleDarkMode = () => {
    const [darkModeEnable, setDarkModeEnable] = useState<string | null>(localStorage.getItem('darkMode') || 'enabled');

    const toggleDarkMode = useCallback(() => {
        const darkModeEnabled = document.documentElement.classList.toggle('dark');
        localStorage.setItem('darkMode', darkModeEnabled ? 'enabled' : 'disabled');
        setDarkModeEnable(darkModeEnabled ? 'enabled' : 'disabled');
    }, []);

    useEffect(() => {
        const savedDarkMode = localStorage.getItem('darkMode') || 'enabled';
        if (savedDarkMode === 'enabled') {
            document.documentElement.classList.add('dark');
        } else {
            document.documentElement.classList.remove('dark');
        }
    }, []);

    return (
        <div className="text-center">
            <ImageButton
                imageWidth={25}
                imageHeight={25}
                imageSrc={darkModeEnable === 'enabled' ? '/sun.svg' : '/moon.svg'}
                imageAlt="Dark Mode"
                imageTitle="Dark Mode"
                onClick={toggleDarkMode}
                classes="flex bg-blue-400 hover:bg-blue-600 dark:hover:bg-blue-300 rounded-full"
            />
        </div>
    );
};

export default memo(ToggleDarkMode);
