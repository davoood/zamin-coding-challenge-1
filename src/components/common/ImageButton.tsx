import React, { FC } from 'react';
import cn from 'classnames';

interface ImageButtonProps {
    onClick: () => void;
    text?: string;
    classes?: string;
    disabled?: boolean;
    imageSrc?: string;
    imageAlt?: string;
    imageTitle?: string;
    imageWidth?: number;
    imageHeight?: number;
}

const ImageButton: FC<ImageButtonProps> = ({
    onClick,
    text,
    imageSrc,
    imageAlt,
    imageTitle,
    classes,
    disabled,
    imageWidth,
    imageHeight,
}) => {
    return (
        <button className={cn('btn items-center justify-between p-3', classes)} onClick={onClick} disabled={disabled}>
            <img src={imageSrc} alt={imageAlt} title={imageTitle} width={imageWidth || 40} height={imageHeight || 40} />
            {text && <span>{text}</span>}
        </button>
    );
};

export default ImageButton;
