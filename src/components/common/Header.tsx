import React, { memo } from 'react';

const Header = () => {
    return (
        <header
            data-testid="header"
            className="sticky top-0 z-10 backdrop-blur flex justify-between bg-white dark:bg-gray-800 shadow-md p-4 w-full mb-5"
        >
            <div>
                <h1 className="text-2xl font-bold text-blue-500 dark:text-blue-300">Adjustable Interval Stopwatch</h1>
                <p className="text-gray-600 dark:text-gray-300">
                    Create and control a versatile stopwatch with adjustable intervals.
                </p>
            </div>
            <div>
                <img src="/stopwatch-blue.svg" alt="stopwatch" title="stopwatch" width={70} height={70} />
            </div>
        </header>
    );
};

export default memo(Header);
