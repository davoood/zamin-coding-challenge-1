import React, { FC } from 'react';
import cn from 'classnames';

interface ButtonProps {
    text: string;
    onClick: () => void;
    classes?: string;
    disabled?: boolean;
}

const Button: FC<ButtonProps> = ({ text, onClick, classes, disabled }) => {
    return (
        <button
            onClick={onClick}
            className={cn('btn p-3 rounded-md text-white', classes)}
            disabled={disabled}
            data-testid={`btn-${text}`}
        >
            {text}
        </button>
    );
};

export default Button;
