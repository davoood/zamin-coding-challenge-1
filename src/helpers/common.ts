export const formatTime = (time: number): string => {
    const hours = Math.floor(time / 3600);
    const minutes = Math.floor((time % 3600) / 60);
    const seconds = time % 60;
    return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds
        .toString()
        .padStart(2, '0')}`;
};

export const copyTextToClipboard = (text: string) => {
    if (navigator.clipboard) {
        navigator.clipboard.writeText(text).then(
            function () {
                alert('در کلیپ بورد کپی شد');
            },
            function () {
                alert('خطا در اشتراک گذاری');
            }
        );
    } else {
        alert('مرورگر شما قابلیت اشتراک گذاری ندارد');
    }
};
